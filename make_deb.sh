#!/usr/bin/env bash

##################
# Define variables
ARCH=amd64              # or i386
DISTRO=jessie           # or wheezy
ERL_VERSION=OTP_R15B03  # or OTP_R15B03

##################
# Make clean enviroment
TMP=`mktemp -d`
cd $TMP
/usr/sbin/debootstrap --variant=minbase --components=main --arch $ARCH $DISTRO . http://httpredir.debian.org/debian
cd ..

##################
# Install required packages
chroot $TMP/ apt-get update
chroot $TMP/ apt-get install -y locales && export LC_ALL=C.UTF-8 && locale-gen C.UTF-8 && /usr/sbin/update-locale LANG=C.UTF-8
chroot $TMP/ apt-get install -y build-essential autoconf m4 libncurses5-dev openssl libssl-dev unixodbc-dev xsltproc checkinstall git

##################
# Clone erlang source repo
chroot $TMP/ git clone git://github.com/erlang/otp.git && cd otp #&& git checkout $ERL_VERSION

##################
# Build
export ERL_TOP=$PWD
export PATH=$ERL_TOP/bin:$PATH
export MAKEFLAGS=-j8
./otp_build autoconf
./configure --enable-threads --enable-smp-support --enable-kernel-poll --enable-sctp --enable-hipe --without-javac
make

##################
# Make deb package
checkinstall --type=debian --install=no

rm -rf $TMP

